from setuptools import setup

setup(
    name='mygithubapp01',
    version='1.0',
    packages=['app_github'],
    long_description=open('README.txt').read(),
    install_requires=[
        'click',
        'PyGithub'
    ],
    entry_points={
        'console_scripts': [
            'mygithub=app_github.cli:main'
        ]
    }
)
