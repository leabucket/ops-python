from github import Github 
import click
import json
import sys
import os
from datetime import datetime


token = os.environ['GITHUB_TOKEN']

def main():
    #get the github instance with token
    gh = Github(token)
    repos = [repo.name for repo in list(gh.get_user().get_repos())]
    print(repos)

