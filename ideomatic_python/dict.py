'''
Construct a dictionary from a key value pair

'''

class Person():
    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname


    def printInfo(self):
        print("Firstname: {} \n Lastname: {} ".format(self.firstname, self.lastname))
        print("END...")
#person = Person('Leadel', 'Kolomongo', 39)

#print(person.printInfo())

# see the person object above
people = [Person("Leadel", "Kolomongo"), Person("Nicole", "Kolomongo")]

person_dic = dict([(p.firstname, p.lastname) for p in people])

print("###")
print(person_dic)
print("###")
print(people[0].firstname)
print(people[1].printInfo(), end='\n<==>')

## The above is equivalent to
# people = [Person("Leadel", "Kolomongo"), Person("Nicole", "Jahns")]
# person_dic = {}
#
# for p in person:
#     person_dic[p.firstname] = p.lastname

names = ["Nick", "Alice", "Kitty"]
professions = ["Programmer", "Engineer", "Art Therapist"]

## To iterate the two arrays above and create a hash between

prof_zip = zip(names, professions)
print(list(prof_zip))

#prof_dic = dict([p[0]])


#print(dict(prof_dict))
#print(dir(prof_dict))
