class Customer(object):
    """
    A Customer class for checking bank account.
    Customer class has the following properties:

    Attributes:
        name: (string) customer name
        balance: (float) account balance
    """
    
    def __init__(self, name, balance=0.0):
        """Returns a customer object with name and balance"""
        self.name = name
        self.balance = balance
    
    def withdraw(self, amount):
        try:
            if amount > self.balance:
                raise RuntimeError('Not possible. Amount is greater than your Balance')
        except RuntimeError:
            print("Not enough credit")
        else:    
            self.balance -= amount
            return self.balance

    def deposite(self, amount):
        self.balance += amount
        return self.balance
        
