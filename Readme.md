IMPORTANT:::
a good tutorial to simply setup a python project:
http://www.academis.eu/posts/pyscaffold
https://pyscaffold.org/en/latest/

Install:
pip install --upgrade pyscaffold

To install it with all extensions:
pip install --upgrade pyscaffold[all]

# So Prepering the Developmet Environment will be as follows
> pip install pyscaffold
> putup myProject
> cd myProject
> python -m venv .venv
> source .venv/bin/activate
> ## edit setup.cfg to add dependencies ..
> pip install -e .

Important.
Make sure you always setup an virtualenv when working with a new project in python.
========> <========
```
virtualenv -p /usr/bin/python3 .venv
source .venv/bin/activate
python setup.py develop --user

```
========> <========
```
virtualenv -p /usr/local/bin/python3 .venv
virtualenv .venv
. .venv/bin/activate
python --version
pip install -e .
aws-backup --region eu-west-1 --environment nonlive dynamodb backup
aws-backup --profile spoc-nonlive-admin --region eu-west-1 --environment nonlive dynamodb backup

```

Or from a jenkins instance or a bash
====>
#!/usr/bin/env bash
. unassume
. assume_live
virtualenv .venv
. .venv/bin/activate
python setup.py install --user
<=====

Important:
https://www.youtube.com/watch?v=kr0mpwqttM0

python Gurru: Raymond Hettinger

```
## using pretty print in python
from pprint import pprint

create dynamic variable names

def create_file(name):
  filename = '{}.log'.format('samba')
  filename2 = '%s.log' % ('wiki')

```
The Decorator function

- Decorator dynamically alter the functionality of a function, method or class without
  having to directly use subclasses
- Ideal when you need to extend the functionality of functions that you don't want to modify.

- Decorators works as Wrappers, modifying the behavior of the code before and after a trigger function execution,
  without the need to modify the function itself, thus decorating it.
VIEW THIS VERY IMPORTANT TUTORIAL

http://nafiulis.me/making-a-static-blog-with-pelican.html
https://medium.com/quick-code/understanding-self-in-python-a3704319e5f0
Python class __init__ and self tutorial:

https://micropyramid.com/blog/understand-self-and-__init__-method-in-python-class/


Class:
  A Blueprint for individual objects with exact behaviour

Object:
  An instance of the class

Self:
  Represents an instance of a class. By using "self" keyword,
  we can access the attributes and methods of the class in Python

__init__:
  Also know as a constructor. This method called when an object is created from the class
  and it allow the class to initialize the attributes of a class

The Python Class and instance Variables:

** Class Variables are defined just after the class definition and outside of any methods
```python

  Class SomeClass:
    variable_1 = "sample1"
    variable_2 = "sample2"

```

** Instance Variable should be defined within methods

```

  Class SomeClass:
    var_1 = "some class variable"

    def __init__(self, param1, param2):
      self.instance_var1 = param1
      self.instance_var2 = param2

```

##String strips and capitalize the first letter of the String

example:

myString = " leadel kolomongo"
print(myString.strip().title())


## list all the methods and variables of the class object

myclass = SomeClass(param1, param2)

dirs(myclass)
=======
dirs(myclass)  # list all methods for myclass
vars(myclass) # list all instance variables of the class

## Add some more elements to a tuple.
## since tuples are immutables, you will have to create a new tuple

```
  tuple1 = ('leadel', 'Nicole')
  tuple2 = tuple1 + ('Jamie',)

  Note the (,) at the end is very important. If you don't add that, you will get the following error:
  >> TypeError: can only concatenate tuple (not "str") to tuple

```

https://medium.com/quick-code/understanding-self-in-python-a3704319e5f0

## Args and Kwargs in python
https://code.tutsplus.com/articles/understanding-args-and-kwargs-in-python--cms-29494

*args are used to pass non-keyword arguments eg func(2,3) or bar('first', 'second')
Use it as a measure to prevent the program from crashing as you do not know how many arguments will be passed to the function

*kwargs is a dictionary of keyword arguments. The ** allows us to pass and number of keyword

## enumerate over an array and it indexe
for index, item in enumerate(items):
    print(index, item)



Python Variables:
python checks variables in the Local scope, then the
enclosing scope, then the Global scope and lastly the Built-in

LEGB
L = Local variables eg variables defined within a function
E = Enclosing eg variables in the local scope of an enclosing function
G = Global eg variables defined at top level
B = Built-in eg names pre assigned in python


Python functions:

********* MAP ********************************************
  map applies a function to all the items in an input list
  The result will be a map object which can be viewed when converted to a list

=======
  ** returned value is a List

  map(function_to_apply, list_of_inputs)

  example:

  items = [1,2,3,4,5]
  squared = list(map(lambda x: x*x, items))

  in the above example, lambda is an anonymous function which takes in  and returns a multiple of x to an array.

  OUTPUT: [1,4,9,16,25]

  Or you can also pass in a list of functions to an expression

  def mult(x):
    return x*x

  def add(x):
    return x+x

  func_array = [mult, add]

  for num in range(5):
    mult_add_values = list(map(lambda x: x(num), func_array))
    print(mult_add_values)


********** FILTER *****************************************
Filters create a list of elements for which an expression/function returns true

** returned value is a List

example:

number_list = range(-5,5)
print(list(number_list))
less_than_zero = list(filter(lambda x: x < 0, number_list))
print(less_than_zero)

Output:
[-5, -4, -3, -2, -1]

********** REDUCE ******************************************
Reduce performes some computation on a list and returns the result.
It applies a rolling computation to a sequential pair of values in a list.

#Reduce accumulate and returns a single result, given a sequence
and passing each value to a function along with the current result

Example:
compute the product of a list of integers

Normal using for loop
product = 1
list = [1,2,3,4,5]

for num in list:
  product = product * num

output = 120

Using the reduce method:
 from functools import reduce
 product = reduce((lambda x, y: x * y), list)


Example2
from functools import reduce
reduce(lambda accum, current: accum + current, [1,2,3,4,5], 0)

zero(0) at the end is the initial value for the result.
default is zero


 ********* *args and **kwargs in python *******************
 The *args and **kwargs allow you to pass a variable number of arguments to a function.
 The names doesn't matter. important is the * and ** infront of the variable name
 *args = non-keyworded variable length

 ```

  def test_arg(f_arg, *argv):
    print('First variable arg is %s' % (f_arg))
    for arg in argv:
      print("Another args are %s" % (arg))

Test:
  test_arg('firstValue', 'firstargs', 'secondArg', 'lastArg', 'and_many_more')

```

**
The **kwarg allows you to pass keyworded variable length of arg to a function

```

 def greet_me(**kwargs):
    for k,v in kwarg.items():
      print('{} = {}' % (k,v))

greet_me(firstname='leadel')


```

The basic syntax is
some_function(function_arg, *args, **kwargs)


*** Debugging in small python scripts ********

```

import pdb

def make_bread():
  pdb.set_trace()
  return 'no time'

print(make_bread())

Or from the commandline

>> python -m pdb script.py

```

Some import debugger commands are:

c: continue execution
w: show context of current line being executed
a: print args list of current function
s: (step) execute the current line and stop
n: (next) continue execution until the next line reaches return

*** Definition of Iterable, Iterator and Iteration ***************

**Iterable:
  This is any object in python which has an __iter__ or a __getitem__ method defined which
  return an iterator or can take indexes.

  An iterable is an object which can provide us with an iterator.
  example list.
  To convert a list to an iterator:
    list.__iter__() ## This will return an iterator.
    iter(list) ## will also return an iterator

**Iterator:
  An iterator is any object which has a next or __next__ method defined.

**Iteration:
  The process of taking an item from an iterator eg list.

************ Generators *************************************

Generators are Iterators, but you can only iterate over them once.
Generators does not store all its values in memory. It generats them on the fly.
Generators do not return a value, they yield it.

- Generators are used either by calling the next method on the
Generator object or using the Generator object in a 'for loop'

```

def generator_function():
  for i in range(10):
    yield i
for item in generator_function():
  print(item)

2.

def fab(n):
  a = b = 1
  for i in range(n):
    yield a
    a,b = b, a + b

To use the above,
  for x in fab(1000):
    print(x)

```
Manipulating ARRAY in PYTHON

arr = [1,2,3,4,5,6,7,8]

arr[:2]
Out: [1, 2]

arr[:6]
Out: [1, 2, 3, 4, 5, 6]

arr[2:6]
Out: [3, 4, 5, 6]

arr[5:6]
Out: [6]

arr[1:6]
Out: [2, 3, 4, 5, 6]

arr[1:6:2]
Out: [2, 4, 6]
 

def count_numbers(count):
    numbers = ['one','two','three']
    for num in numbers[:count]:
       yield num
          

count_any = lambda x: count_numbers(x)
list(count_any(2))


************ Ternary Operators **************************
most commonly known as conditional expressions in python.
Blueprint:

condition_is_true if condition else condition_is_false

my own works:
 returned_value if condition_is_true else returned_value_if_condition_is_false
example:

is_fat = True
state = 'The boy is Fat' if is_fat else 'not fat'


************* Decorators in python *********************

Decorators are functions that modify the functionality of another function.


**** Use the __slots__ to reduce RAM in python.
By default, python uses dict to store an objects instance attributes. Helpful
for setting abitrary new attributes at runtime.

BUT: dicts waste alot of RAM.
To avoid that, use  __slots__ to tell python not to use dict and
only allocate space for a fixed set of attributes.

```

Example Without:
class MyClass():
  def __init__(self, name, age):
    self.name = name
    self.age = age
    self.setup()

```

```

Example With:

class MyClass():
  __slots__ = ['name', 'age']
  def __init__(self, name, age):
    self.name = name
    self.age = age
    self.setup()

```
*** General Template for Dict comprehension **

my_dict = {key:value for (key,value) in dictionary.items()}


## List comprehension in python3

[ val for val in collection ]
[ val for val in collection if <condition> ]
[ val for val in collection if <condition1> and <condition2> ]
[ val1,val2 for val1 in collection1 and val2 in collection2 ]

example:
find all movies that start with the letter 'G' into a list

gmovies = [title for title in movies if title.startswith("G") ]


example2:
  names_amount = [('sam', 100), ('kan', 300), ('bala', 10)]
  # get the names with amounts less than 100
  naless = [name for (name, amount) in names_amount if amount < 100 ]

A = [1,2,3]
B = [4,5,6]

cartesian_product = [(a,b) for a in A for b in B]



## Get your own Ip in python using the request module

import requests

response = requests.get('https://httpbin.org/ip')

print('Your IP is {0}'.format(response.json()['origin']))

-----------------------------------------------------------

## In order to keep your python environment consistent, it is a good idea to FREEZE the
current state of the environment packages.

>> pip freeze > requirements.txt

To install from the freezed packages

>> pip install -r requirements.txt


## Example project layout.
mypackage will be the name of our project

A good convention will be to name your package after your project name


mypackage/
  bin/
  CHANGES.txt
  docs/
  LICENSE.txt
  MANIFEST.in
  README.txt
  setup.py
  mypackage/
    __init__.py
    module1.py
    module2.py
    tests/
      __init__.py
      test_module1.py
      test_module2.py
    submodule01/
      __init__.py
    submodule02/
      __init__.py
    docs/
      index.rst
      conf.py
      Makefile

So the the above case, a sample setup.py file content will be as follows:

##setup.py

from setuptools import setup, find_packages
setup(name="mypackage",
      version="0.1",
      packages=find_packages(exclude=['tests', 'tests.*']),
      install_requires=[
        'numpy',
        'click',
        'json'
      ],
      test_suits="tests",
      entry_points={
        'console_scripts':[
          'run=mypackage.module1:run'
        ]
      }

)

# After configuring your setup.py file
>> python setup.py install ## --user if you get errors

During development
>> python setup.py develop --user


## to Run all your test in the test folder
>> python setup.py test

## Build documentation
>>python setup.py docs


## Working with the json module

```
json.load(f) ==> allows you to load json data from a file
json.loads(s) ==> load json data from a String

```
################### Python regular expression example #######################

```
my_str='''
111-222-333-4444
222.333.444.5555
'''
import re
pattern = re.compile()
pattern = re.compile(.)
pattern = re.compile(r'.')
pattern??
pattern.findall(my_str)
pattern.findall??
help(pattern.findall)
pattern.finditer(my_str)
list(pattern.finditer(my_str))
pattern = re.compile(r'\d\d\d')
list(pattern.finditer(my_str))
my_str[9:12]
result = list(pattern.finditer(my_str))
result[0]
result[0].span()
result[0].match
result[0].match()
result[0].end()
result[0].start()
my_str[result[0].start():result[0].end()]
for match in list(pattern.finditer(my_str)):
    print(my_str[match.start():match.end()])

```
####################### Using jmespath to search key value pairs from a json document #######################

```
example_json='''
{'_id': '0',
 '_index': 'twitter',
 '_type': '_doc',
 'explanation': {'description': 'weight(message:elasticsearch in 0) [PerFieldSimilarity], result of:',
  'details': [{'description': 'score(doc=0,freq=1.0 = termFreq=1.0\n), product of:',
    'details': [{'description': 'idf, computed as log(1 + (docCount - docFreq + 0.5) / (docFreq + 0.5)) from:',
      'details': [{'description': 'docFreq', 'details': [], 'value': 1.0},
       {'description': 'docCount', 'details': [], 'value': 5.0}],
      'value': 1.3862944},
     {'description': 'tfNorm, computed as (freq * (k1 + 1)) / (freq + k1 * (1 - b + b * fieldLength / avgFieldLength)) from:',
      'details': [{'description': 'termFreq=1.0', 'details': [], 'value': 1.0},
       {'description': 'parameter k1', 'details': [], 'value': 1.2},
       {'description': 'parameter b', 'details': [], 'value': 0.75},
       {'description': 'avgFieldLength', 'details': [], 'value': 5.4},
       {'description': 'fieldLength', 'details': [], 'value': 3.0}],
      'value': 1.2222223}],
    'value': 1.6943599}],
  'value': 1.6943599},
 'matched': True}'''

myjson = json.loads(example_json)

import jmespath
jmespath.search('explanation.description', myjson)
jmespath.search('details[0].description', myjson)
myjson['details']
myjson['explanation']['details']
myjson['explanation']['details'][0]
myjson['explanation']['details'][0]['details']
jmespath.search('explanation.details[0].description', myjson)
jmespath.search('explanation.details[1].description', myjson)
jmespath.search('explanation.details[0].description', myjson)

```

## On the other side, you can also use the compile function as in re

searchString = jmespath.compile('explanation.description')
found = searchString.search(myjson)

"String FOUND" if found else 'String not Found'

