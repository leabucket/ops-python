import logging

logger = logging.getLogger(__name__)
#print(logger.name)

def foo():
    logger.info("Hi Foo")

class Bar(object):
    def bar(self):
        logger.info("Hi Bar")

