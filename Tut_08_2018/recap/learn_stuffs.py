def cal_values():
    return lambda x: x+x

cal = cal_values()

print(cal(3))

print(cal_values()(5))

func0 = lambda x,y: x+y
print(func0(2,3))

fullname = lambda fname,lname: "Welcome {0} {1}".format(fname.strip().title(),lname.strip().title())
print(fullname(' leadel', ' Kolomongo'))

scifi_authors = ["Isaac mus", "Ray Brad", "kelle sam", "Douglas Adam", "Frank Herbert", "Charly chin"]
print(scifi_authors)
scifi_authors.sort(key = lambda name: name.split(" ")[0].lower() )
print(scifi_authors)

## list comprehension

mylist = [ x for x in range(10)]
print(mylist)
mynames = [name for name in scifi_authors]

# for name in mynames:
#     print(name, sep="##")