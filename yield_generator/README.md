Python Learing tutorial:   https://realpython.com/



Tutorial for Learning phython yield and generators

1: https://jeffknupp.com/blog/2013/04/07/improve-your-python-yield-and-generators-explained/


Phython class and object oriented programming

https://jeffknupp.com/blog/2014/06/18/improve-your-python-python-classes-and-object-oriented-programming/


Iterating over a generator object

iterator_obj = iter([1,2,3,4,5])

while True:
  try:
    num = next(iterator_obj)
    print(num)
  except StopIteration:
    break

## Working with linux exceptions
## Note that the try block will throw a AssertionError exception
## if the value not found in sys.platform

import sys

 try:
     assert ('Bla' in sys.platform), "This Code"
 except AssertionError as e:
     print("This code runs only on linux")

OUTPUT:     
This code runs only on linux

 try:
     assert ('Bla' in sys.platform), "This Code"
 except AssertionError as e:
     print("This code runs only on linux")
 else:
     print("Woow this line prints cuz you are on linux")
 finally:
     print("END of code. Cleaning up")

OUTPUT:         
This code runs only on linux
END of code. Cleaning up

 try:
     assert ('linux' in sys.platform), "This Code"
 except AssertionError as e:
     print("This code runs only on linux")
 else:
     print("Woow this line prints cuz you are on linux")
 finally:
     print("END of code. Cleaning up")

OUTPUT:         
Woow this line prints cuz you are on linux
END of code. Cleaning up

 try:
     assert ('lonux' in sys.platform), "This Code"
 except AssertionError as e:
     print("This code runs only on linux")
 else:
     print("Woow this line prints cuz you are on linux")
 finally:
     print("END of code. Cleaning up")

OUTPUT:         
This code runs only on linux
END of code. Cleaning up


## Create a simple generator object ####

mylist = [1,2,3,4,5]
gen_obj = (x for x in mylist)

for val in gen_obj:
print(val)