import os

def generate_filenames():
    """
    generates a sequence of opened files
    matching a specific extension
    """

    for dir_path, dir_names, file_names in os.walk('.'):
        for f in file_names:
            if f.endswith('.py'):
                yield open(os.path.join(dir_path, f))


def cat_files(files):
    """
    takes in an iterable of filenames
    """

    for fname in files:
        for line in fname:
            yield line

def grep_files(lines, pattern=None):
    for line in lines:
        if pattern in line:
            yield line


py_files = generate_filenames()
py_file = cat_files(py_files)

#print(list(py_file))
lines = grep_files(py_file, 'ENDPOS')

for line in lines:
    print(line)
