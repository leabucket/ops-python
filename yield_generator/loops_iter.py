num = [1, 2, 3, 4, 5]

for item in num:
    print (item)
print("---------")
for item in num:
    if item == 3:
        raise Exception("Exception FOUND")
    print(item)

num_iter = iter(num)
ENDPOS = False

print("######## Generator example###############")
while True:
    try:
        if not ENDPOS:
            print('next value of the generator')
            print(next(num_iter))
    except StopIteration:
        ENDPOS = True
        break
