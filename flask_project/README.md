## Setup

Create a python3 virtual environemnt, install dependencies
and run the flask development server

```bash
virtualenv -p $(which python3) .venv
. .venv/bin/activate
pip install -e .
export FLASK_APP=ecgithub
export FLASK_ENV=development
export SETTINGS=$(pwd)/conf/nonlive.py
export AWS_PROFILE=$YOUR_AWS_PROFILE
export AWS_REGIO=$YOUR_AWS_REGION
flask run
```
Create a virtual env and work

export PIPENV_VENV_IN_PROJECT=1
pipenv install --three --dev
pipenv shell # This will activate the virtual environment

# Run a program in your virtual environment
pipenv run pip install awscli
