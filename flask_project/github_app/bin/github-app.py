from IPython import embed
import boto3
from github import Github
import click
import os

def cli():
    github_token = os.environ['GITHUB_TOKEN']
    github_client = Github(github_token)

    header = """
        Welcome to my private github.
    """ 

    embed(header=header)

if __name__ == '__main__':
    cli()