import boto3

s3 = boto3.resource('s3')

for bucket in s3.buckets.all():
    print(bucket.name)

data = open('../cheeseshop.py', 'rb')

s3.Bucket('opswebsite').put_object(Key='cheeseshop.py', Body=data)