'''
This file will show you also some example of working with timezones

'''

import datetime
import pytz
import os
from lotto_generator import LottoEuroGenerator

with open(os.path.join(os.getcwd(), 'test.log'), 'r') as fd:
    print(fd.mode)

date_utcnow = datetime.datetime.now(tz=pytz.UTC)
print(date_utcnow)

lottogen = LottoEuroGenerator('Nic')
lotto = lottogen.genNumbers('lotto')
lotto(8)