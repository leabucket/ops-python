import os

'''This os module enables us to interact with the underline operating system'''

'''To show all the attributes and methods of a module, use the dir function '''

# for value in dir(os):
#     print(value)

## print the current working directory

print(os.getcwd())

## Navigate to a new location

os.chdir('/tmp')

print(os.getcwd())

## List all files and folders in the /tmp directory

print(os.listdir())

## create a new folder. Use os.makedirs('dir_name') which will create
## a directory and all its subdirectory like mkdir -p in shell
## os.mkdir() no subdir create
os.makedirs('/tmp/sub1/sub2')

## to delete the directory and its subdirs
## os.rmdir() no remove of subdir
os.removedirs('tmp/dir1/dir2')

## get information about a file

os.stat('file.txt')
## get the size of the file
print(os.stat('file.txt').st_size)

## get the last modification time of a file

from datetime import datetime

mod_time = os.stat('file.txt').st_mtime
print(datetime.fromtimestamp(mod_time))

## os.walk() traverse a directory. It returns 3 arguments
## dirpath, dirname, filename

for dirpath, dirnames, filenames in os.walk('/tmp'):
    print('Content Path:', dirpath)
    print('Directories', dirnames)
    print('Files:', filenames)
    print()
