# import random
#
# #print(random.randint(1,49))
# lotto_complete = {}
#
# for i in range(1,6):
#     lotto_numbers = []
#     for val in range(6):
#         rand_number = random.randint(1,49)
#         if rand_number not in lotto_numbers:
#             lotto_numbers.append(rand_number)
# #print(lotto_numbers)
#     num_name = 'Number' + str(i)
#     lotto_complete[num_name] = lotto_numbers
#
# print("the complete set is {}".format(lotto_complete))


import random
from functools import wraps

"""
    Generates Lotto and Eurojackpot numbers
    Lotto numbers generates a combination of 6 out of 49 numbers
    Eurojackpot generates a combination of 5 out of 50 numbers + zusatz numbers of 2 out of 10
"""

class LottoEuroGenerator:

    def __init__(self, name):
        self.name = name


    def welcomeMessage(self):
        print('Welcome Mr. {}'.format(self.name))

    def goodByeMessage(self):
        print('Thanks for using my Program. Hope to see you next time Mr. {}'.format(self.name))

    def lottoGenerator(self, numberOfCombinations):
        lottoNumbers = 6
        self.welcomeMessage()
        for num in range(numberOfCombinations):
            print('Lotto Number {}: {}'.format(num + 1, sorted(random.sample(range(1,49), lottoNumbers))))
        self.goodByeMessage()

    def euroGenerator(self, numberOfCombinations):
        euroNumbers = 5   #choose 5 out of 50
        zsatNumbers = 2   #choose 2 out of 10

        def zusatznumbers():
            print('Zusatz_Number: {}'.format(sorted(random.sample(range(1,10), zsatNumbers))))

        self.welcomeMessage()
        
        for num in range(numberOfCombinations):
            print('EuroJackpot Number_{}: {}'.format(num+1, sorted(random.sample(range(1,50), euroNumbers))))
            zusatznumbers()

        self.goodByeMessage()

        
    def genNumbers(self, gen):
        return self.euroGenerator if gen == 'euro' else self.lottoGenerator

if __name__ == '__main__':
    lottogen = LottoEuroGenerator('Leadel')

    lotto = lottogen.genNumbers('lotto')
    lotto(5)

    print('############', end='\n')

    euro = lottogen.genNumbers('euro')
    euro(4)



""" lNumberOfCombination = 3
lNumberCount = 5

zNumberCount = 2
zNumberOfCombination = 2

def lotto_number():
    for num in range(lNumberOfCombination):
        print("Lotto_Number{}: {}".format(num, sorted(random.sample(range(1,50), lNumberCount))))
        zusat_number()
    print("Thanks you for using the Lotto Generator")

def zusat_number():
    for num in range(zNumberOfCombination):
        print("Zusat_Number: {}".format(sorted(random.sample(range(1,10), zNumberCount))))


def main():
   lotto_number()

main()
 """

# import random
#
# def generate():
#     lottery_list = []
#     for i in range(7):
#         lottery_list.append(random.randrange(0 ,10))
#     print(lottery_list)
#     return lottery_list
# def display(lottery_list):
#     for i in lottery_list:
#         print(i)
# lottery_list = generate()
# display(lottery_list)
