def choose_class(classname):
    if classname == 'foo':
        class Foo():
            pass
        return Foo #return the class and not an instance of it
    else:
        class Bar():
            pass
        return Bar 
MyClass = choose_class('foo')
print(MyClass)

print("\nPrint out an Object of the above class")
print(MyClass())

#Using the type build-in method to dynamically create a class

MyDynamicClass = type('MyDynamicClass', (), {'Firstname': 'Leadel', 'Lastname': 'Kolomongo'})
print("\n")
print(MyDynamicClass)
print(MyDynamicClass())
print("\n")
print(dir(MyDynamicClass))
print(MyDynamicClass.Firstname)
