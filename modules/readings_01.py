'''
Source: https://swcarpentry.github.io/python-novice-inflammation/10-cmdline/

** Questions
How can I write Python programs that will work like Unix command-line tools?

** Objectives
- Use the values of command-line arguments in a program.

- Handle flags and files separately in a command-line program.

- Read data from standard input in a program so that it can be used in a pipeline.
  
**This is a python program that will do the following:

- Prints the average inflammation per patient for a giving file
- if no file is given on the commandline, read the data from stdin
- if one or more filenames are given, read data from them and report statistics
    for each file seperately
- Use the --min, --mean, or --max flag to determine what statistic to print

How to call the program:

>> python readings_04.py --mean inflammation-01.csv

Or the min of the first four lines

>> head -4 inflammation-01.csv | python readings_04.py --min

Or the max inflammations in serveral files one after another

>> python readings_04.py --max inflammation-*.csv

'''

import sys
import numpy

