class Employee():
    def __init__(self, name, age, salary):
        self.name = name
        self.age = age
        self.salary = salary

    def __repr__(self):
        return '({}, {}, ${})'.format(self.name, self.age, self.salary)
    
emp1 = Employee('firstEmployee', 29, 3000)
emp2 = Employee('secondEmployee', 30, 2300)
emp3 = Employee('lastEmployee', 32, 4000)
emp4 = Employee('blaEmployee', 32, 4000)
emp5 = Employee('wickedEmployee', 32, 4000)
emp6 = Employee('AnalogEmployee', 32, 4000)

employee_list = [emp1, emp2, emp3, emp4, emp5, emp6]

def printList(em_list):
    for em in em_list:
        print(em)

with open('test.log', 'w') as fd: 
    for emp in employee_list:
        print(emp, file=fd)

print('Unsorted list of employee')
printList(employee_list)

print("\n################## Sorted list")
##Variant-1
def sort_employee(emp):
    return emp.name

sorted_employee = sorted(employee_list, key = sort_employee)

printList(sorted_employee)

# Now to the above sort by age using a lambda function
##Variant-2 using lambda
sort_em_age = sorted(employee_list, key = lambda e: e.age)
print("Sorted Employee list per age", end='\n\n')
printList(sort_em_age)

## variant-3 using the attrgetter function from operator module

from operator import attrgetter