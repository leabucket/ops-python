#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A Simple python script to show you how to use the Click module in python.
Instead of using the argparse mode, click provides a simple interface for
setting up a command line script.

Note:
1. Create a virtualenv and activate it
    >> virtuelenv .venv
    >> source .venv/bin/activate
2. Install your dependencies
    >> pip install -e .
3. Build your package
    >> python setup.py build
4. Install your package
    >> python setup.py install --user
5. Run you command
    >> myclick
    >> myclick --version
"""
import sys
import logging

from clickproject import __version__

__author__ = "Leadel Ngalame"
__copyright__ = "Leadel Ngalame"
__license__ = "mit"

_logger = logging.getLogger(__name__)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point allowing external calls

    """
    setup_logging(args)
    _logger.debug("Starting crazy calculations...")

    _logger.info("Script ends here")


def run():
    """Entry point for console_scripts

    """
    print("#######")
    main('DEBUG')


if __name__ == "__main__":
    run()
