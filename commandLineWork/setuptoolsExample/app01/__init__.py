from . import cli

def testMain():
    print("Welcome")
    cli.main()

## To test the above in ipython shell
## import app01
## app01.testMain()
## Or you can as well acces the cli funtion as you import it above
## app01.cli.main()
