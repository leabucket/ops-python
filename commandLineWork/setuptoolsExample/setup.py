from setuptools import setup

setup(
    name="setuptoolsExample",
    version="0.1",
    description="My sample commandline application",
    author="leadel@localhost",
    packages=['app01'],
    install_requires=[
        'click'
    ],
    entry_points={
        'console_scripts': [
            'run_myapp=app01.cli:main'
        ]
    }
)
