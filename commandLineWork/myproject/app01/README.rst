=====
app01
=====

My first app project

Description
===========

This project is a sample project to demonstrate the use of setup tools.


Note
====
The project structure

app01/
  docs/
  src/
  tests/
  setup.cfg
  setup.py
