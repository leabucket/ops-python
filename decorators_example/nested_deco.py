from functools import wraps

def logit(logfile='out.log'):
    def log_decorator(func):
        @wraps(func)
        def wrapper_func(*args):
            print(func.__name__ + " was called and will be decorated")
            with open(logfile, 'a') as fd:
                fd.write('Logs from function ' + func.__name__)
                fd.write('\n')
        return wrapper_func
    return log_decorator

@logit()
def myfunc1():
    print('myfunc1 will be logged..')

@logit()
def myfunc2():
    print('myfunc2 will be logged...')

for i in range(10):
    myfunc1()
    myfunc2()