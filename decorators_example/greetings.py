def greet(name):
    return "Mr/Mrs "+name

## Assign a function to a variable
greet_someone = greet

print ("Hallo %s" % greet_someone("Leadel"))

## Define a function inside another function

def newLine():
    print ("*"*50)


def greet2(name):
    def get_message():
        return 'Hello'

    return get_message()+ " " +name

print (greet2("Johnny"))


def persons(firstname, lastname, age):
    def printInfo():
        return "Hello " +firstname+ " "+lastname+ " "+str(age)
    message = "This are informations of the person %s" %firstname

    return message + "\n" + printInfo()

print (persons("Leadel", "Kolomongo", 38))

newLine()

def call_func(func):
    other_name = "Nicole"
    return func(other_name)

print (call_func(greet2))

newLine()

def another_func_call(func):
    firstname = "Joy"
    lastname = "Ngalame"
    age = "13"

    return func(firstname, lastname, age)

print (another_func_call(persons))
