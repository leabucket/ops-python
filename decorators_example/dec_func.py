def decorator_function(original_function):
    def wrapper_function(*args, **kwargs):
        print('From wrapper {}'.format(original_function.__name__))
        return original_function(*args, **kwargs)
    return wrapper_function

@decorator_function
def display():
    print('I just ran from display and will be wrapped')

@decorator_function
def info(fname, age):
    print('Just ran with args {} {}'.format(fname, age))

display()
info('Leadel', 39)