'''
show you how you use decorators in python
you will see how to return a function from another function 
'''

def generalGreetings(name, sex):
    if sex not in ['male', 'female']:
        print('sex can only be male OR female')
        exit (2)
    
    state = 'Mr' if sex == 'male' else 'Mrs'

    def greet():
        return 'welcome %s %s. Personal greeting for you' % (state, name)
    
    def welcome():
        return 'welcome %s %s' % (state, name)
    
    if name.lower() == 'leadel':
        return greet
    else:
        return welcome 



print("Now we are going to welcome Mr leadel")

salut = generalGreetings(name='LEADEL', sex='male')
print(salut())

print('\n')

print('Now we are going to welcome Nicole')

salut_nic = generalGreetings('Nicole', 'female')
print(salut_nic())
    
print('Code will exit cuz sex not male or female')

unknown_sex = generalGreetings('Balajoe', 'unknown')
print(unknown_sex())