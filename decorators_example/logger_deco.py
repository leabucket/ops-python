from functools import wraps
'''
The wraps module will allow you to preserve the function name
being decorated. Especially used when stacked on another.

'''

def my_logger(orig_function):
    import logging
    logging.basicConfig(filename='{}.log'.format(orig_function.__name__), level=logging.INFO)

    @wraps(orig_function)
    def wrapper(*args, **kwargs):
        logging.info(
            'Ran with args: {} and kwargs: {}'.format(args,kwargs)
        )
        return orig_function(*args, **kwargs)
    return wrapper

def my_timmer(orig_function):
    import time
    @wraps(orig_function)
    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = orig_function(*args, **kwargs)
        t2 = time.time() - t1

        print('{} ran in {} sec'.format(orig_function.__name__, t2))
        return result
    return wrapper

@my_timmer
def display_function(name, age):
    print('display ran with args {} {}'.format(name, age))


display_function('Leadel', 39)