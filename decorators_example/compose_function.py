def newLine():
    print "*"*50

def compose_greet_func(name):
    def get_message():
        return "Helo there "+name+"!"
    return get_message()

print compose_greet_func("Johnny")
