def newline():
    print("*" * 50)


def get_text(name):
    return "Hallo my string is {0}".format(name)


def p_decorate(func: object) -> object:
    def func_wrapper(name):
        return "<p>{0}</p>".format(func(name))

    return func_wrapper


## To have get_text itself to be decorated as p_decorate
## we have to assign get_text to the result of p_decorate

my_get_text = p_decorate(get_text)
print(my_get_text)
print(dir(my_get_text))
print(my_get_text("Nicole"))

names = "Leadel Ngalame"

#print("my name is {}".format(names))

## Another variant of the above code could be

# def p_decorate(func):
#     def func_wrapper():
#         return "<p>{0}</p>".format(func)
#     return func_wrapper
#
# my_get_text = p_decorate(get_text("Jamie"))
#
# print my_get_text()

# The python decorator syntax
# we do not have to do my_get_text = p_decorate(get_text)
# instead use a shortcut to mention the name of the decorating function before
# the function to be decorated. The name of the decorator should be appended with @

newline()


def p_decorator2(func):
    def func_wrapper2(name):
        return "<p>{0}</p>".format(func(name))

    return func_wrapper2


@p_decorator2
def get_text2(name):
    return "Hallo from {}".format(name)


print(get_text2("Leadel"))
