class Decorator_class(object):
    def __init__(self, original_function):
        self.original_function = original_function

    def __call__(self, *args, **kwargs):
        print('Call method executes this {}'.format(self.original_function.__name__))
        return self.original_function(*args, **kwargs)

## Now use the above class decoratore to decorate the below functions

@Decorator_class
def display():
    print('Display func ran')

@Decorator_class
def display_info(name, age, type='sys-admin'):
    print('display_info ran with args: {} {} {}'.format(name, age, type))

display()
print('++++++++++++++++++++++++++')
print('++++++++++++++++++++++++++')
display_info('Leadel', 38)