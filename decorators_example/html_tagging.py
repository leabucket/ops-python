def html_tag(tag):

    def wrap_text(message):
        print('<{0}>{1}<{0}>'.format(tag, message))

    return wrap_text

h1_tag = html_tag('h1')
print(h1_tag)
print(dir(h1_tag))
print(h1_tag.__name__)

print(h1_tag('Hello Mr. Leadel'))
