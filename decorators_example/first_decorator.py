'''
This is my first decorator code
@ is just a short way of making a decorator function.
'''

def a_new_decorator(a_func):
    
    def wrapTheFunction():
        print('Doing some work before executing a_func()')

        a_func()

        print('Work Done :)')

    return wrapTheFunction

def say_myName():
    print('my name is Leadel')

a_new_decorator(say_myName)()
#or
myName_deco = a_new_decorator(say_myName)
myName_deco()

print('###################################')
print('Above example decorator using the @ symbol')
print('###################################')

def deco_with_arg(func):
    def wrapper_with_arg(name):
        print('Trying to make you not nored again')
        func(name)
        print('Well DONE. Everything went on well')

    return wrapper_with_arg

## NOTE: the below is same as
## need_deco = deco_with_arg(i_need_decoration)
## need_deco('Leadel')
@deco_with_arg
def i_need_decorator(name):
    print('Hi Mr. {}, i am feeling bored and need decorator'.format(name))

i_need_decorator('Leadel')

print(i_need_decorator.__name__)

'''
You will notice that when i print out the name of the function i_need_decorator,
it outputs wrapper_with_arg
If you want to get back the name of a fuction been wrapped, use the functools.wraps function

Example:
 
from functools import wraps

def a_new_deco(func):
    @wraps(func)
    def wrapFunction():
        print('Do something before call')
        func()
        print('End')
    return wrapFunction

@a_new_deco
def func_to_deco():
    print('I need some deco')

print(func_to_deco.__name__)

BLUEPRINT

from functools import wraps

def decorator_name(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not can_run:
            return "Function will not run"
        return f(*args, **kwargs)
    return decorated

@decorated_name
def func():
    return("function is running ...")

can_run = True
print(func()) ##Outputs function is running

can_run = False
print(func()) ##Outputs Function will not run

'''