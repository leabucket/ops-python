#!/usr/bin/python

import sys

print(sys.argv)

for i in range(len(sys.argv)):
    if i == 0:
        print('Function name is = {0}'.format(sys.argv[i]))
    else:
        print('Argument: {0} = {1}'.format(i,sys.argv[i]))
