input_numbers = []

input_number = 0

while input_number >= 0:
    input_number = int(input('Please enter a number. -ve number to exit:'))
    if input_number > 0:
        input_numbers.append(input_number) 
    else:
         continue

if len(input_numbers) > 0:
    bigger = sorted(input_numbers, reverse=True)[0]
    smaller = sorted(input_numbers)[0]
    print("BIGGER: %d  SMALLER:%d" % (bigger, smaller))
else: 
    print('Nothing entered. Thanks')
    exit(0)
