def func(*args):
    for arg in args:
        print (arg)

#func(1,2,3,'tut','leadel', {'Firstname':'Leadel', 'Lastname':'Kolomongo'})
vars = [1,2,3,'tut','leadel', {'Firstname':'Leadel', 'Lastname':'Kolomongo'}]

func(vars)
print('-------------------------------------------------------------------')
func(*vars)

## Note:
## When using *args and **kwargs in the same function, *args must appear before **kwargs

#Example

def func2(*args, **kwargs):
    for arg in args:
        print(arg)
    for key,value  in kwargs.items():
        print(key ,"====>", value)


func2('names','firstname','lastname',firsttname = 'leadel',lastname = 'kolo')