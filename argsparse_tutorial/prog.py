import argparse
import logging


AWS_REVERSE_PROXY_TEMPLATES_GIT_REPO = "aws-reverse-proxy-templates"
HAPROXY_TMP_CONFIG = "/tmp/haproxy_tmp_config"
CONFIG_NFS_DIR = "/media/shared/ops/cp/aws-reverse-haproxy"
PROXY_TYPES = ["ib", "pa"]
PUPPETDB_HOST = "puppetdb-querynodes-mesos.lhotse.ov.otto.de"
PUPPETDB_PORT = 443
PUPPETDB_PROTO = "https"

parser = argparse.ArgumentParser(description='update proxy and set Varnish configuration')
parser.add_argument(
    '--mesos-group',
    dest='mesos_group',
    required=True,
    help='mesos_group [ops-ci, develop, live].'
)
parser.add_argument(
    '--aws-reverse-proxy-config',
    dest='aws_reverse_proxy_config',
    default=AWS_REVERSE_PROXY_TEMPLATES_GIT_REPO,
    help='The aws reverse proxy config location [default]: %s.' % AWS_REVERSE_PROXY_TEMPLATES_GIT_REPO
)
parser.add_argument(
    '--config-nfs-dir',
    dest='config_nfs_dir',
    default=CONFIG_NFS_DIR,
    help='The config directory [default]: %s.' % CONFIG_NFS_DIR
)
parser.add_argument(
    '--dry-run',
    dest='dry_run',
    default=False,
    action='store_true',
    help='No varnish or haproxy has to be installes locally [default]: false.'
)
parser.add_argument(
    '--haproxy',
    default='/usr/sbin/haproxy',
    required=False,
    help='Path to HAProxy binary',
)

args = parser.parse_args()
print(args.echo)
#parser.parse_args()