class Person:
    def __init__(self,fname,lname,age):
        self.__firstname = fname
        self._lastname = lname
        self.age = age


    def display_info(self):
        print ("My name is " + self.__firstname + " " + self._lastname + " of age " + self.age )
#        print ("self_tutorial name is: {}".format('__name__'))
#        print (self)

    def math_operation(self, operator, *args):
        print ("Hallo %s. The result is %f " % (self.__firstname, operator(*args)))

def add(*args):
    sum = 0

    for i in args:
      sum = sum + i

    return sum

person01 = Person("Leadel","Kolomomgo", "34")
person02 = Person("Nicole", "Kolomomgo", "23")
person01.display_info()
person01.math_operation(add, 2, 3, 2, 1, 4, 5, 10)
person02.math_operation(add, 3, 3,10)

#display methods and variables of the object
#print (dir(person01))
