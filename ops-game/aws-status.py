import sys
import os
import logging
import boto3
import json
import base64
from time import time, sleep
from functools import wraps

## set a global loging instance
logger = logging.getLogger(__name__)

class AwsStatus:

    def time_it(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            start = time()
            result = f(*args, **kwargs)
            duration = time() - start
            if (duration > 1):
                logger.warning("Execution of '%s' took %d sec." % (f.__name__, duration))
            return result
        return wrapper

    @time_it
    def get_status_json(self, status, reason, version):
        sleep(2)
        return json.dumps({
            "type": "status",
            "environment": "live",
            "status": status,
            "reason": reason,
            "version": version,
            "time": time()
        })

status = AwsStatus()

print (status.get_status_json("OK", "Everything is OK", "10011"))
