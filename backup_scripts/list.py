mylist = []
name = "Leadel Kolomongo"
age = 38

mylist.append(1)
mylist.append(2)

print(mylist[0])

print("------------")

for x in mylist:
    print(x)

print("Hallo Mr {} of age {:d}".format(name, age))

a, b = 0, 1
while b < 100:
    print(b, end='==>')
    a, b = b, a + b

print("END\n")

def create_file(name):
    if name:
        logfile = '{}.log'.format(name)
        with open(logfile, 'w') as lf:
            lf.write('Date:')
## call: create_file('example')