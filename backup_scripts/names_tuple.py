from collections import namedtuple

#NOTE: tuples are imutable and cannot be changed when created
#
Color = namedtuple('Color', ['red', 'green', 'blue'])
# the namedtuple will take the name of the tuple and some list of vars

## Now create an instance of it
myColors = Color(55, 10, 22)
print(myColors.red)
print(myColors[0])