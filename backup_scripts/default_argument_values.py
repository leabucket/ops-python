def ask_ok(prompt, retries=4, reminder='Please try again!'):
    while True:
        ok = input(prompt)

        if ok in ('y', 'Y', 'yes', 'Yes'):
            return True
        if ok in ('n', 'N', 'no', 'No'):
            return False
        retries = retries - 1

        if retries < 0:
            raise ValueError('Invalid user Response')

        print(reminder)

ask_ok('Do You want to Play (yes/no): ')
